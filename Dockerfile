# docker build -t confluence:5.8.15 .
FROM centos:latest
MAINTAINER stoleas <stoleas@users.noreply.github.com>

ENV ARTIFACT_LOCATION http://10.27.102.4/tmp

###################################
# DOWNLOAD SYSTEM UPDATES
###################################
RUN yum -y -q update

###################################
# DOWNLOAD AND PLACE CONFLUENCE
###################################
RUN mkdir -p /atlassian/confluence/
RUN curl ${ARTIFACT_LOCATION}/atlassian-confluence-5.8.15.tar.gz > /atlassian/confluence/atlassian-confluence-5.8.15.tar.gz
RUN tar -xzf /atlassian/confluence/atlassian-confluence-5.8.15.tar.gz -C /atlassian/confluence && rm /atlassian/confluence/atlassian-confluence-5.8.15.tar.gz
RUN ln -s /atlassian/confluence/atlassian-confluence-5.8.15/ /atlassian/confluence/current

###################################
# MOVE CONFIGURATION FILE TEMPLATES
###################################
ADD confluence/bin/daemon.sh /atlassian/confluence/current/bin/daemon.sh
ADD confluence/bin/setenv.sh /atlassian/confluence/current/bin/setenv.sh
ADD confluence/bin/user.sh /atlassian/confluence/current/bin/user.sh
ADD confluence/conf/server.xml /atlassian/confluence/current/conf/server.xml
ADD confluence/confluence/WEB-INF/classes/confluence-init.properties /atlassian/confluence/current/confluence/WEB-INF/classes/confluence-init.properties

EXPOSE 8000

# docker run \
#        --expose 8000 \
#        --expose 8090 \
#        -p 8090:8090 \
#        -v /data/java:/data/java \
#        -v /data/confluence_data1:/data/confluence-home \
#             -ti confluence:5.8.15 /bin/bash